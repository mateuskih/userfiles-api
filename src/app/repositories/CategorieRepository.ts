import { Repository, EntityRepository } from 'typeorm';
import Categorie from '../models/Categorie';

@EntityRepository(Categorie)
class CategorieRepository extends Repository<Categorie> {}

export default CategorieRepository;
