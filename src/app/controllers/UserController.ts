import { Request, Response } from 'express';
import { getCustomRepository } from 'typeorm';
import User from '../models/User';
import BookRepository from '../repositories/BookRepository';
import FileRepository from '../repositories/FileRepository';
import UserRepository from '../repositories/UserRepository';

class UserController {
  static index(req: Request, res: Response) {
    return res.send({ userId: req.userId });
  }

  static async create(req: Request, res: Response) {
    const repository = getCustomRepository(UserRepository);
    const { name, email, password, role } = req.body;

    const userExists = await repository.findOne({ where: { email } });

    if (userExists) {
      return res.status(409).json({ message: 'E-mail ja cadastrado' });
    }

    const user = repository.create({
      name,
      email,
      password,
      role
    });
    await repository.save(user);

    delete user.password;

    return res.status(201).json(user);
  }

  static async find(req: Request, res: Response){
    const repository = getCustomRepository(UserRepository);
    const { id } = req.body;

    const user = await repository.findOne({ where: { id } });
    if(!user){
      return res.status(400).json({ message: 'Id nao encontrado' })
    }
    res.status(201).json(user);
  }

  static async listAll(req: Request, res: Response){
    const repository = getCustomRepository(UserRepository);
    
    const users = await repository.query('SELECT * FROM users');    
    res.json({ users })
  }

  static async file(req: Request, res: Response){
    const repository = getCustomRepository(BookRepository);
    const id = req.userId;

    const book = await repository.find({ relations: ["user", "file"]});
    
    const updatedBook = book.filter(book => book.user.id === id)
    const updatedFiles = updatedBook.map(book => book.file)

    if(!updatedFiles){
      return res.status(400).json({ message: 'File nao encontrado' })
    }

    res.json(updatedFiles);
    
  }
}

export default UserController;
