declare namespace Express {
    export interface Request {
        userId: string
        bookId: string
        pageId: string
        categorieId: string
    }
}