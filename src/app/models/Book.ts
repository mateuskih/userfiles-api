import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
  ManyToMany,
  JoinTable,
} from 'typeorm';

import Page from './Page';
import Categorie from './Categorie';
import File from './File';
import User from './User';

@Entity('books')
class Book {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  author: string;

  @Column()
  releaseDate: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column({ default: 'https://i.imgur.com/EQfyCXg.png' })
  cover: string;

  @ManyToOne(() => User, (user) => user.books)
  user: User;

  @ManyToMany(() => Categorie, (categorie) => Categorie)
  @JoinTable()
  categories: Categorie[];

  @OneToMany(() => Page, (page) => page.book)
  pages: Page[];

  @OneToMany(() => File, (file) => file.book)
  file: File[];
}

export default Book;
