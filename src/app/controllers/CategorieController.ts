import { Request, Response } from 'express';
import { getCustomRepository } from 'typeorm';
import CategorieRepository from '../repositories/CategorieRepository';

class CategorieController {
  static async createCategorie(req: Request, res: Response) {
    const { name } = req.body;

    const repository = getCustomRepository(CategorieRepository);

    const post = repository.create({
      name,
    });

    await repository.save(post);

    return res.json(post);
  }

  static async findCategorie(req: Request, res: Response) {
    const repository = getCustomRepository(CategorieRepository);
    const { id } = req.params;

    const categorie = await repository.findOne({ where: { id } });
    if (!categorie) {
      return res.status(400).json({ message: 'Categorie nao encontrado' });
    }
    res.status(201).json(categorie);
  }

  static async listCategories(req: Request, res: Response) {
    const repository = getCustomRepository(CategorieRepository);
    const categories = await repository.find();
    return res.json(categories);
  }

  static async deleteCategorie(req: Request, res: Response) {
    const repository = getCustomRepository(CategorieRepository);
    const categorie = await repository.findOne(req.params.id);

    if (!categorie) {
      return res.status(400).json({ message: 'Page nao encontrado' });
    }

    await repository.remove(categorie!);
    return res.json(categorie);
  }
}

export default CategorieController;
