import { Repository, EntityRepository } from 'typeorm';
import Page from '../models/Page';

@EntityRepository(Page)
class PageRepository extends Repository<Page> {}

export default PageRepository;
