import { Request, Response } from "express";
import { Any, getCustomRepository, In } from 'typeorm';
import Book from "../models/Book";
import BookRepository from '../repositories/BookRepository';
import CategorieRepository from "../repositories/CategorieRepository";
import PageRepository from "../repositories/PageRepository";
import UserRepository from "../repositories/UserRepository";


class BookController {

    static async createBook (req: Request, res: Response) {

        const { author, releaseDate, title, description, cover, } = req.body
        const userReq = req.userId;
        const categorieReq = req.body.categorieId;
        

        const userRepository = getCustomRepository(UserRepository);
        const user = await userRepository.findOne(userReq);

        const categorieRepository = getCustomRepository(CategorieRepository);

        const categories = await categorieRepository.find({ where: { name: In(categorieReq)} });

        console.log(categorieReq)
        console.log(categories)
        const repository = getCustomRepository(BookRepository);
        const post = repository.create({
            author,
            releaseDate,
            title,
            description,
            cover,
            user: user,
            categories: categories,
        })

        await repository.save(post);

        return res.json(post);
      
    }

    static async findBook(req: Request, res: Response){
        const repository = getCustomRepository(BookRepository);
        const { id } = req.params;
    
        const book = await repository.findOne({ where: { id } });
        if(!book){
          return res.status(400).json({ message: 'Book nao encontrado' })
        }
        res.status(201).json(book);
    }

    static async listBooks (req: Request, res: Response){
      const repository = getCustomRepository(BookRepository);
      const books = await repository.find({ relations: ["user", "file"]});
      return res.json(books);
   }

   static async deleteBook(req: Request, res: Response) {
    const repository = getCustomRepository(BookRepository);
    const book = await repository.findOne(req.params.id);

    if (!book) {
      return res.status(400).json({ message: 'Page nao encontrado' });
    }

    await repository.remove(book!);
    return res.json(book);
  }

}

export default BookController;