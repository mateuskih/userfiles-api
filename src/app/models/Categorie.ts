import {
    Column,
    Entity,
    ManyToMany,
    ManyToOne,
    PrimaryGeneratedColumn,
  } from 'typeorm';
import Book from './Book';
  
  @Entity('categories')
  class Categorie {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    name: string;
    
  }
  
  export default Categorie;
  