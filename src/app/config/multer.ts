  
import multer from "multer";
import path from "path";
import crypto from "crypto";
import md5 from 'md5';


const storageTypes = {
    local:  multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, path.resolve(__dirname, '..', '..', '..', 'tmp', 'uploads'))
        },
        filename: (req, file, cb) => {
            crypto.randomBytes(16, (err: Error, hash) => {
                if (err){
                    cb (err, err.toString());
                };
                const fileName = `${hash.toString('hex')}-${file.originalname}`;
                cb(null, fileName);
            }) 
        }
    })
}

export default module.exports = {
    dest: path.resolve(__dirname, '..', '..', 'tmp', 'uploads'),
    storage: storageTypes['local'],

    limits: {
        fileSize: 2 * 1024 * 1024 // == 2mb;
    },
    fileFilter: (req, file, cb) => {
        const allowedMimes = [
            'image/jpeg',
            'image/pjpeg',
            'image/png',
            'application/pdf'
        ];

        if(allowedMimes.includes(file.mimetype)){
            cb(null, true)
        } else {
            cb(new Error('Invalid File Type.'));
        }
    }
}