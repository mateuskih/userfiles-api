import { Repository, EntityRepository } from 'typeorm';
import Book from '../models/Book';

@EntityRepository(Book)
class BookRepository extends Repository<Book> {}

export default BookRepository;
