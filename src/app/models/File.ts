import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
  PrimaryColumn,
  BeforeInsert,
  AfterRemove,
  BeforeRemove,
} from 'typeorm';

import fs from 'fs';
import path from 'path';
import { promisify } from 'util';

import Book from './Book';

@Entity('files')
class File {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  key: string;

  @Column()
  url: string;

  @Column()
  size: Number;

  @Column()
  name: string;

  @Column()
  description: string;

  @ManyToOne(() => Book, (book) => book.file)
  book: Book;

  @CreateDateColumn()
  created_at: Date;

  @BeforeInsert()
  setUrl() {
    if (!this.url) {
      this.url = `http://localhost:3333/files/${this.key}`;
    }
  }

  @BeforeRemove()
  deleteLocalUpload() {
    const pathUrl = path.resolve(
      __dirname,
      '..',
      '..',
      'tmp',
      'uploads',
      this.key
    );
    console.log(pathUrl);
    return promisify(fs.unlink)(
      path.resolve(__dirname, '..', '..', '..', 'tmp', 'uploads', this.key)
    );
  }
}

export default File;
