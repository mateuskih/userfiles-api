import { Request, Response } from 'express';
import { getCustomRepository } from 'typeorm';
import BookRepository from '../repositories/BookRepository';
import PageRepository from '../repositories/PageRepository';

class PageController {
  static async createPage(req: Request, res: Response) {
    const { text, pageNumber } = req.body;

    const bookReq = req.params.bookId;
    const bookRepository = getCustomRepository(BookRepository);
    const book = await bookRepository.findOne(bookReq);

    const repository = getCustomRepository(PageRepository);

    const post = repository.create({
      text,
      pageNumber,
      book,
    });

    await repository.save(post);

    return res.json(post);
  }

  static async findPage(req: Request, res: Response) {
    const repository = getCustomRepository(PageRepository);
    const { id } = req.params;

    const page = await repository.findOne({ where: { id } });
    if (!page) {
      return res.status(400).json({ message: 'Page nao encontrado' });
    }
    res.status(201).json(page);
  }

  static async listPages(req: Request, res: Response) {
    const bookReq = req.params.bookId;
    const bookRepository = getCustomRepository(BookRepository);
    const book = await bookRepository.findOne(bookReq);

    const repository = getCustomRepository(PageRepository);
    const pages = await repository.find({ where: { book: book } });
    return res.json(pages);
  }

  static async deletePage(req: Request, res: Response) {
    const repository = getCustomRepository(PageRepository);
    const page = await repository.findOne(req.params.id);

    if (!page) {
      return res.status(400).json({ message: 'Page nao encontradp' });
    }

    await repository.remove(page!);
    return res.json(page);
  }
}

export default PageController;
