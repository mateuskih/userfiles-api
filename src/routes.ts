import { Request, Response, Router } from 'express';

import multer from 'multer';
import multerConfig from './app/config/multer';
import authMiddleware from './app/middlewares/authMiddleware';
import UserController from './app/controllers/UserController';
import AuthController from './app/controllers/AuthController';
import adminMiddleware from './app/middlewares/adminMiddleware';
import { getCustomRepository } from 'typeorm';
import FileController from './app/controllers/FileController';
import CategorieController from './app/controllers/CategorieController';
import PageController from './app/controllers/PageController';
import BookController from './app/controllers/BookController';
import filehelper from './app/config/file-helper'

const router = Router();


//router.post('/upload', multer(multerConfig).single("file"), DocumentController.createDocument);
//router.get('/upload', DocumentController.listDocuments);
//router.get('/upload/:id', DocumentController.findDocumentsById);
//router.get('/users/documents', authMiddleware, DocumentController.findUserDocument);


/*router.post('/book/:bookId/file',  multer(multerConfig).single("file"), FileController.createFile);*/

router.get('/', (res, req) => req.send("API Rodando..."));

router.post('/user', UserController.create);
router.post('/user/login', AuthController.authenticateLogin);
router.get('/users', adminMiddleware, UserController.listAll);
router.get('/user', adminMiddleware, UserController.find);
router.get('/user/validate', authMiddleware, UserController.index);
router.get('/user/files', authMiddleware, UserController.file);

router.post('/book/:bookId/file', multer(multerConfig).single("file"), (req, res, next) => {
    filehelper.compressImage(req.file, 500)
    FileController.createFile(req, res)
})
router.get('/book/:bookId/file/:id', authMiddleware, FileController.findFileById);
router.get('/book/:bookId/files', authMiddleware, FileController.listFiles);
router.delete('/book/:bookId/file/:id', authMiddleware, FileController.deleteFile);

router.post('/book', authMiddleware, BookController.createBook);
router.get('/book/:id', authMiddleware, BookController.findBook);
router.get('/books', authMiddleware, BookController.listBooks);
router.delete('/book/:id', authMiddleware, BookController.deleteBook);

router.post('/book/:bookId/page', authMiddleware, PageController.createPage);
router.get('/page/:id', authMiddleware, PageController.findPage);
router.get('/book/:bookId/pages', authMiddleware, PageController.listPages);
router.delete('/page/:id', authMiddleware, PageController.deletePage);

router.post('/categorie', adminMiddleware, CategorieController.createCategorie);
router.get('/categorie/:id', authMiddleware, CategorieController.findCategorie);
router.get('/categories', authMiddleware, CategorieController.listCategories);
router.delete('/categorie/:id', adminMiddleware, CategorieController.deleteCategorie);

export default router;
