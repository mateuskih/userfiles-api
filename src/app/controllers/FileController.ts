import FileRepository from '../repositories/FileRepository';
import {request, Request, Response} from 'express';
import { getCustomRepository } from 'typeorm';
import File from '../models/File';
import BookRepository from '../repositories/BookRepository';

class FileController {

    static async createFile (req: Request, res: Response) {
        let {originalname: name, size, filename: key } = req.file;
        const bookreq = req.params.bookId;
        
        const bookRepository = getCustomRepository(BookRepository);
        const book = await bookRepository.findOne(bookreq);
        const repository = getCustomRepository(FileRepository);

        const allowed = ['png', 'jpg', 'jpeg'];
		if(allowed.includes(name.split('.')[1])){
			name = name.split('.')[0] + '.webp';
			key = key.split('.')[0] + '.webp';
		}

        const post = repository.create({
            name,
            key,
            size,
            book: book,
            url: '',
            description: '',
        })

        await repository.save(post);

        return res.json(post);
        
    }

    static async findFileById (req: Request, res: Response){
        const repository = getCustomRepository(FileRepository);
        const id = req.params.id;
        const file = await repository.find({ where: { id: id } });
        if(!file){
            return res.status(400).json({ message: 'File nao encontrado' })
        }
        res.json(file);
    }

    static async listFiles (req: Request, res: Response){
        const repository = getCustomRepository(FileRepository);
        const id = req.params.bookId;

        const bookRepository = getCustomRepository(BookRepository);
        const book = await bookRepository.findOne(id);

        const files = await repository.find( { where: { book: book } } );
        return res.json(files);
    }

    static async deleteFile (req: Request, res: Response){
        const repository = getCustomRepository(FileRepository);
        const file = await repository.findOne(req.params.id);

        if(!file){
        return res.status(400).json({ message: 'File nao encontrado' })
        }
        await repository.remove(file!);
        return res.json(file);
    }
    
}

export default FileController;