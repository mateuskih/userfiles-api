import {
    Column,
    Entity,
    ManyToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
  } from 'typeorm';
  import Book from './Book';
  
  @Entity('pages')
  class Page {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    text: string;

    @Column()
    pageNumber: number;

    @ManyToOne(() => Book, (book) => book.pages)
    book: Book;
  }
  
  export default Page;
  